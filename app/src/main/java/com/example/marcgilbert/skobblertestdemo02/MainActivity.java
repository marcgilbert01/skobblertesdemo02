package com.example.marcgilbert.skobblertestdemo02;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.skobbler.ngx.SKMaps;
import com.skobbler.ngx.SKMapsInitSettings;
import com.skobbler.ngx.SKMapsInitializationListener;
import com.skobbler.ngx.map.SKAnnotation;
import com.skobbler.ngx.map.SKCoordinateRegion;
import com.skobbler.ngx.map.SKMapCustomPOI;
import com.skobbler.ngx.map.SKMapFragment;
import com.skobbler.ngx.map.SKMapPOI;
import com.skobbler.ngx.map.SKMapSurfaceListener;
import com.skobbler.ngx.map.SKMapSurfaceView;
import com.skobbler.ngx.map.SKMapViewHolder;
import com.skobbler.ngx.map.SKPOICluster;
import com.skobbler.ngx.map.SKScreenPoint;
import com.skobbler.ngx.packages.SKPackage;
import com.skobbler.ngx.packages.SKPackageManager;
import com.skobbler.ngx.packages.SKPackageURLInfo;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;

import cz.msebera.android.httpclient.Header;


public class MainActivity extends Activity {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    Context context;
    Handler handler = new Handler();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        context = this;


        SKMapsInitSettings skMapsInitSettings = new SKMapsInitSettings();
        skMapsInitSettings.setConnectivityMode(SKMaps.CONNECTIVITY_MODE_OFFLINE);



        final SKMaps skMaps = SKMaps.getInstance();
        skMaps.initializeSKMaps(getApplication(), new SKMapsInitializationListener() {
            @Override
            public void onLibraryInitialized(boolean b) {

                //Toast.makeText(getApplicationContext(), " initialized = "+b, Toast.LENGTH_LONG).show();
                // showMap();
                downloadLondon();

            }
        }, skMapsInitSettings);


        // SET BUTTON LISTENER
        findViewById(R.id.buttonGoToMapView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent( context , MapActivity.class );
                startActivity(intent);

            }
        });



    }


    public void downloadLondon() {


        // CHECK IF ALREADY INSTALLED / DOWNLOADED
        SKPackage[] packages = SKPackageManager.getInstance().getInstalledPackages();
        boolean londonAlreadyInstalled  = false;
        for(SKPackage skPackage : packages  ){
            if( skPackage.getName().equals("GBCITY04") ){
                londonAlreadyInstalled = true;
            }
        }


        if( londonAlreadyInstalled ) {

            // DISPLAY ALREADY INSTALLED
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "London already installed", Toast.LENGTH_LONG).show();
                }
            });

            // DISPLAY BUTTON "GO TO MAP"
            handler.post(new Runnable() {
                @Override
                public void run() {

                    findViewById(R.id.buttonGoToMapView).setVisibility(View.VISIBLE);
                }
            });

        }
        else{

            // DISPLAY DOWNLOADING
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context,"Downloading London", Toast.LENGTH_LONG).show();
                }
            });


            // DOWNLOAD
            final String offlinePackageDir = getFilesDir()+"/skobblermaps/london";
            new Thread(){

                @Override
                public void run() {
                    super.run();
                    // DOWNLOAD FILE FROM OUR SERVER
                    final File londonMapFile = new File( offlinePackageDir+"/GBCITY04.skm" );
                    try {

                        downloadFile("http://test.sumitanantwar.com/maps/GBCITY04.skm", londonMapFile, new Callback() {
                            @Override
                            public void onDone() {



                                    // ADD OFFLINE MAP TO SDK
                                    SKPackageManager.getInstance().addOfflinePackage(  offlinePackageDir  , "GBCITY04");





                            }
                        });
                        // DISPLAY BUTTON "GO TO MAP"
                        handler.post(new Runnable() {
                            @Override
                            public void run() {

                                findViewById(R.id.buttonGoToMapView).setVisibility(View.VISIBLE);
                            }
                        });

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                }
            }.start();

        }

    }



/////////// DOWNLOAD METHOD
/////////////////////////////////////////////////
    interface Callback {

        public void onDone();

    }

    private void downloadFile(String urlStr, File file, Callback callback) throws MalformedURLException {

        URL url = new URL(urlStr);
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        FileOutputStream fileOutputStream = null;


        try {

            // CONNECT TO URL
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            // CREATE FILE
            if( file.exists() ){
                file.delete();
            }
            file.getParentFile().mkdirs();
            file.createNewFile();

            long contentLenght = 0;
            long downloaded = 0;
            try {
                contentLenght = Long.parseLong(httpURLConnection.getHeaderField("Content-Length"));
            }catch (Exception e){}


            // COPY URL DATA TO FILE
            inputStream = httpURLConnection.getInputStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            fileOutputStream = new FileOutputStream(file);
            final byte data[] = new byte[1024];
            int count;
            while ((count = bufferedInputStream.read(data, 0, 1024)) != -1){
                fileOutputStream.write(data, 0, count);
                downloaded += count;
                //System.out.print( " urlrrrrrdownloaded "+downloaded+" bytes" );
            }

            callback.onDone();


        } catch (Exception e) {
            e.printStackTrace();

        }finally {

            if (inputStream != null) try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (fileOutputStream != null) try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if( httpURLConnection!=null ){
                httpURLConnection.disconnect();
            }
        }

    }


}
